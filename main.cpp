#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
#include <windows.h>
#include "xor.h" // <-- cifrador Xor!

using namespace std;

void EnviarMensajes(){ //funci�n para cifrar y guardar contenido
     
     string msj;
   
     ofstream datos("mensajes.txt", ios::app); // crea y abre un nuevo archivo txt llamado mensajes.txt
     cout<<"Enviar mensaje:\n";
     getline(cin, msj); // se optiene el contenido original...
   
     string contenido = Xor(msj); //... y luego cifra en Xor dicho contenido.
     datos<<contenido<<"\n"; // guarda el contenido cifrado en el txt
     datos.close();
   
}; // fin del cifrador.

void Descifrar(){ // funci�n para descifrar y mostrar el contenido
   
     string msj;
    
     ifstream datos; // crea el objeto 'datos' para abrir el nuestro archivo mensajes.txt
     datos.open("mensajes.txt"); 
    
     if (datos.fail()) { // si no existe el archivo, muestra un mensaje
        cerr<<"No hay mensajes.\n"<<endl;
     }
       
     while (getline(datos, msj)){ // muestra en pantalla todo el contenido de mensajes.txt descifrado.
     string contenido = Xor(msj);
     cout<<contenido<<endl;
     }
    
     datos.close(); // cierra el txt
     
}; // fin de la funci�n para descifrar.

void Chat(){ // esta funci�n trabajr� con dos las funciones anteriores recreando un Chat o Mensajeria de texto.
     
     char resp;
     
     system("cls");
     Descifrar(); // <-- Muetra mensajes
     do{
        cout<<"Mensaje? (s/n) r para actualizar. \n"; // pregunta si desea trabajar o salir
        resp = getch();
        if(resp == 's' || resp == 'S'){ // si la respuesta es afirmativa...
                system("cls");          // limpia la panatlla
                Descifrar();            // muestra la linea de mensajes
                EnviarMensajes();       // y despliega la opci�n para escribir contenido.
        }
        if(resp == 'r' || resp == 'R'){ // si la respuesta es r para actualizar
              Chat();                   // llama a la misma funcion y reinicia dando el efecto de un chat.
        }
     }while(resp == 's' || resp == 'S');

}; // Fin

void Pantalla(int src){ // <-- pantallas para la interfaz de usuario
     
     switch(src){
         case 1:
              cout<<"***************************"<<endl;
              cout<<"**.:|              8:48am**"<<endl;
              cout<<"**                       **"<<endl;
              cout<<"**     Adrian Olmedo     **"<<endl;
              cout<<"**                       **"<<endl;
              cout<<"**        [Menu]         **"<<endl;
              cout<<"***************************"<<endl;
              break;
         case 2:
              cout<<"***************************"<<endl;
              cout<<"**.:|              8:48am**"<<endl;
              cout<<"**                       **"<<endl;
              cout<<"**[1] Mensajes.          **"<<endl;
              cout<<"**[2] Salir.             **"<<endl;
              cout<<"**[3] Apagar.            **"<<endl;
              cout<<"***************************"<<endl;
              break;
         case 3:
              cout<<"***************************"<<endl;
              cout<<"**                       **"<<endl;
              cout<<"**                       **"<<endl;
              cout<<"**       Apagando...     **"<<endl;
              cout<<"**                       **"<<endl;
              cout<<"**                       **"<<endl;
              cout<<"***************************"<<endl;
              break;
     }

};

void InterfazUsuario(){ // funci�n principal para operar toda la interfaz del usuario (simulando un tel�fono),
                        // este desplegar� el men� de opciones donde trabajr� con las funciones 
                        // anteriormente establecidas: Pantalla(), Chat() y Apagar().

   int intro; char opc, resp;
   
   do{
      system("cls");
      system("color 0A");
      Pantalla(1); // Pantalla Inicial
      intro = getch();
      do{
          do{ 
             system("cls");
             Pantalla(2); // Menu de opciones
             opc = getch();
          }while(opc < '1' || opc > '3'); // fin del ciclo do-while para el men� de opciones
        
          switch(opc){
                 
              case '1': Chat(); break;
                    
              case '2': InterfazUsuario(); break; // <-- Inicia otra vez la funci�n y se repite el ciclo.
              
              case '3': 
                   system("cls");
                   system("color 08");
                   Pantalla(3); // Pantalla de apagado
                   Sleep(2000);
                   exit(0);
                   break;
            
        } // fin del switch
        
      }while(opc != '3'); // fin del ciclo do-while para las funciones del men� de opciones
   }while(intro == '\n'); // fin del ciclo do-while de la pantalla principal
        
}; // fin de la funci�n de toda la interfaz del usuario

main(){ 
   InterfazUsuario(); // S�lo queda llamar la funcion principal y har� el resto...
}
