#include <iostream>
#include <string>
using namespace std;
 
string Xor(string Encryptar) {
    char key = 'K'; // Cualquier letra funciona!
    string salida = Encryptar;
   
    for (int i=0; i < Encryptar.size(); i++) // Se aplica un incrementador 
        salida[i] = Encryptar[i] ^ key; // Aqu� se aplica la disyuncion exclusiva para que la computadora
                                        // haga el trabajo binario.
   
    return salida;
}
